# Day Three

* JavaScript -> what on earth is it?  Why is the most popular language in the world?  Who uses it? How is it used? What does it look like?
* HTML & CSS -> How does JS compliment HTML & CSS?
* Application Lifecycle Development

# What is JavaScript?

* Softly typed language (Weakly typed in days gone by)
* Used to make websites more interactive
* Object-oriented, absolutely. Also, not.
* Client-side and server-side applications

**JavaScript is the most popular language in the world sitting at 51% of the world using it**

* JavaScript is split up into many libraries and frameworks, both client-side and server-side development
    * Vanilla (no libraries, no frameworks, just JS), React.js, Vue.js, Angular.js, Ember.js, Ligthning.js, Node.js, Deano.js, and many more

* JavaScript is very forgiving. You can make mistakes but due to its inferred nature, it lets you off.

## JavaScript Fundamentals

### Data Types

* string -> handles the same as C# does for strings except you can use "", '', ``
* number -> both integer and decimal numbers
* boolean -> true / false
* null -> nothing. nada. zip.
* undefined -> means it doesn't know what it is

**How do we define these data types?**
```js

let myFirstName = "Jacob" // this will return Jacob
let myAge = 56 // this will return 56
let isThatTrue = true // return true
let nothing = null // return null

```

## Functions

* Functions are methods that exist outside of a class
* Methods are functions inside of a class

## == vs ===

* == only checks the value of the answer
* === checks the value and the type of the answer

# CSS

* Three different ways of using CSS

1. Inline CSS -> this is bad. You shouldn't use inline CSS
2. Internal CSS -> this isn't as bad. But don't use it... if you can avoid it.
3. External CSS -> use this.

# Application Lifecycle Management - Software Development Lifeycycle

* Set of procedures that we follow during our development of an application

0. Feasibility Study
    * Should you do the project?
    * Is it financially viable? 
    * Do you have the resources? (Staff, Skills, Software etc) 
    * Do you have the time? 
    * Will you gain from the project?
    * Is this company the right fit?
1. Requirements
    * What are the details needed for the application to be successful?
    * What are the key points?
    * What languages are needed?
    * Setting expectations
        * How long do we have?
        * Who do we have to develop it?
        * Where will we do this? -> cloud, on-premises, SaaS
    * Business Analyst will be responsible
    * Project Managers
2. Design
    * The technology stack
        * C#, SQL, HTML etc
    * User Interface -> blueprint
    * User Experience:
        * Efficiency of design
        * Efficiency of use
    * Technical Documentation
    * Simple User Documentation
        * Documents
        * Videos
        * Snippets
        * Samples / Templates 
            * Functions
            * Code blocks etc
3. Development
    * Put the design into practice
    * Write the code:
        * Software
        * Database
        * Any other connections we may have
    * Write the documentation
4. Testing
    * Checking / verifying that software works against the requirements
    * Software testing helps find defects - what it cannot do is guarantee the absence of defects
    * Testing is massive
    * Unit Testing - anything that interacts / holds or can hold a value in your code
    * Exhaustive testing -> testing everything and ensuring it is working
    * White-box -> testing an application with full exposure to the inner-workings of the application and its structure
    * Black-box -> testing an application with no care for the inner-workings of the application, only the input and the output

    * When do you stop testing? When you have an MVP (Minimum viable product)?

5. Maintenance / Deployment

    * How long do you maintain the product for?
    * What's the scope of the maintenance?
    * Who is going to maintain it?
    * How are you going to maintain it?
    * Why are you maintaining it?
    * If passing a project on, how are you going to train the new staff?

