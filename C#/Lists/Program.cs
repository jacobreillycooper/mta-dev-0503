﻿using System;
using System.Collections.Generic;

namespace Lists
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] groupOfItems = { "James Blunt", "Lady Gaga" };
            List<string> favouriteArtists = new List<string>();

            // Add and AddRange
            favouriteArtists.Add("Miley Cyrus");
            favouriteArtists.AddRange(groupOfItems); // Add James Blunt and Lady Gaga
            favouriteArtists.Add("Eminem");

            // Insert and insertRange
            favouriteArtists.Insert(2, "50 Cent"); // which is the 2nd index?
            favouriteArtists.Insert(1, "_Oasis"); // which is the 2nd index?
            favouriteArtists.InsertRange(0, groupOfItems);

            favouriteArtists.Remove("Lady Gaga"); // removes a specific element
            favouriteArtists.Remove("Lady Gaga"); // removes a specific element

            favouriteArtists.RemoveAt(0); // which item will this remove? 

            favouriteArtists.Sort();// numerical then alphabetically
            favouriteArtists.Reverse();

            Console.WriteLine(favouriteArtists.Count);
            Console.WriteLine(favouriteArtists.Contains("Lady Gaga"));
            favouriteArtists.Clear();

            foreach (string item in favouriteArtists)
            {
                Console.WriteLine(item);
            }
        }
    }
}
