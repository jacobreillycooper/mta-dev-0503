﻿using System;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //                              0               1           2       3      
            string[] favouriteArtists = { "Miley Cyrus", "Enimem", "50 Cent", "Hannah Montana", "Lady Gaga" };
            //              4

            Console.WriteLine(favouriteArtists[2]); // which one will return?
            Console.WriteLine(favouriteArtists[3]); // Hannah Montana

            favouriteArtists[3] = "Hugh Jackman";
            Console.WriteLine(favouriteArtists[3]); // What will come back for this one?

            favouriteArtists[3] = "Billy-Ray";
            Console.WriteLine(favouriteArtists[3]); // What will come back for this one?

            // Console.WriteLine(favouriteArtists[5]); // what will this return?
            // Index was outside the bounds of the array. -> we tried to access the array but a value that didn't exist

            // favouriteArtists[5] = "James Blunt"; // what will happen here?
            // Index was outside the bounds of the array
            // just because we can type it, doesn't mean it will work
            // we cannot expand the size of the array

            // we cannot remove items from the array but we can make them blank / empty
            favouriteArtists[3] = "";

            Console.WriteLine(favouriteArtists[0]);
            Console.WriteLine(favouriteArtists.Length); // 5

            // we are creating a variable to be used inside of the array
            // check the value of that variable against the length of the array
            for(int i = 0; i < favouriteArtists.Length; i++)
            {
                // going through the favouriteArtists[0][1][2][3][4]
                Console.WriteLine(favouriteArtists[i]);
            }

            // long way... not really needed anymore
            string[] favouriteColours = new string[4] { "orange", "purple", "blue", "crimson"};


        }
    }
}
