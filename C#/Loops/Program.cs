﻿using System;

namespace Loops
{
    class Program
    {
        static void Main(string[] args)
        {
            // int largestNumber = 100;

            // for (int i = 0; i < largestNumber; i++)
            // {
            //     Console.WriteLine(i);
            // }

            string[] favouriteArtists = { "Miley Cyrus", "Enimem", "50 Cent", "Hannah Montana", "Lady Gaga", "Jimmy Blunt", "Goerge Ezra" };

            // Console.WriteLine(favouriteArtists.Length);
            // for (int i = 0; i < favouriteArtists.Length; i++)
            // {
            //     Console.WriteLine(favouriteArtists[i]);
            //     // each time the loop runs, it will print the index value of the array. First is Miley Cyrus, then Enimem, 50 Cent and so on
            // }

            // foreach (string item in favouriteArtists)
            // {
            //     Console.WriteLine(item); // 
            // }

            int randomNumber = 10;
            while (randomNumber < favouriteArtists.Length)
            {
                Console.WriteLine(favouriteArtists[randomNumber]);
                randomNumber++;
            }

            do
            {
                Console.WriteLine("This will run");
                randomNumber++;
            } while (randomNumber < 100);

            
        }
    }
}
