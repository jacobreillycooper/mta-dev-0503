﻿using System;
using System.Collections.Generic;

namespace Queues
{
    class Program
    {
        static void Main(string[] args)
        {
            // Queues
            Queue<string> favouriteArtists = new Queue<string>();

            favouriteArtists.Enqueue("Miley Cyrus");
            favouriteArtists.Enqueue("George Ezra");

            favouriteArtists.Dequeue(); // remove Miley Cyrus from the queue

            // Peek will return the value at the front of the queue but won't do anything else... you have to do something with it
            string frontOfQueue = favouriteArtists.Peek();
            Console.WriteLine(frontOfQueue); // George Ezra
            Console.WriteLine(favouriteArtists.Peek()); // George Ezra

            Console.WriteLine(favouriteArtists.Contains("Miley Cyrus")); // false because I removed Miley Cyrus on line 16


            int total = favouriteArtists.Count;
            
            Console.WriteLine(favouriteArtists.Count);

            favouriteArtists.Clear();

        }
    }
}
