﻿using System;
using System.Collections.Generic;

namespace Stacks
{
    class Program
    {
        static void Main(string[] args)
        {

            // Please create a stack of favouriteVehicles
            // use push, pop, peek, clear and contains

            // For Robert :-) and anyone fancying a challenge:
            // create a stack for integers not strings for favouriteNumbers or something else that holds numbers
            
            // push, pop, peek, contains, clear
            Stack<string> favouriteArtists = new Stack<string>();
            Stack<int> favouriteNumbers = new Stack<int>();

            favouriteNumbers.Push(1);
            favouriteNumbers.Pop();
            favouriteNumbers.Push(1);
            int topOfTheNumbers = favouriteNumbers.Peek(); // 1
            favouriteNumbers.Clear();
            
            // Console.WriteLine(favouriteArtists); // this confirms that this is a stack

            // Push (add a new item)
            favouriteArtists.Push("Miley Cyrus");
            // Console.WriteLine(favouriteArtists.Count);
            favouriteArtists.Push("Enimem");
            // Console.WriteLine(favouriteArtists.Count);

            // Contains
            // Console.WriteLine(favouriteArtists.Contains("Miley Cyrus")); // true
            // Console.WriteLine(favouriteArtists.Contains("Miley")); // false

            // Peek
            // Console.WriteLine(favouriteArtists.Peek()); // the value can only be used once
            string topOfTheStack = favouriteArtists.Peek(); // stores the value and lets me use the value whenever I want
            // Console.WriteLine(topOfTheStack); // using it whenever I want

            // Console.WriteLine(favouriteArtists.Contains(topOfTheStack)); // 
            // Enimem   
            // Console.WriteLine(favouriteArtists.Contains("enimem".ToUpper())); // what will this say?   

            // Pop -> Removes the item from the stack
            Console.WriteLine(favouriteArtists.Count);         
            favouriteArtists.Pop(); // 1 item remains
            Console.WriteLine(favouriteArtists.Count);  
            favouriteArtists.Pop(); // 0 items remain
            Console.WriteLine(favouriteArtists.Count);  
            // favouriteArtists.Pop(); // Stack Empty error message
            // Console.WriteLine(favouriteArtists.Count);  

            // clear
            favouriteArtists.Push("Miley Cyrus");
            favouriteArtists.Push("Eminem");
            favouriteArtists.Push("50 Cent");
            favouriteArtists.Push("James Blunt");
            Console.WriteLine(favouriteArtists.Count);  // will this be 4 or 1?
            favouriteArtists.Clear(); 
            // Console.WriteLine(favouriteArtists.Count); 

            foreach (string item in favouriteArtists)
            // for each item in favouriteArtists, show me!
            {
                Console.WriteLine(item);
                // james blunt, 50 cent, eminem, miley cyrus
            }

            // Stacks don't index, so we just look from the top - down

        }
    }
}
