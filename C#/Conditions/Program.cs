﻿using System;


namespace Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
           
            string favouriteArtist = "Miley Cyrus";

            if (favouriteArtist == "Miley Cyrus")
            {
                Console.WriteLine("She came in like a wrecking ball");
            }
            else if (favouriteArtist == "Eminem")
            {
                Console.WriteLine("They want Shady.");
            }
            else
            {
                Console.WriteLine("Ummm.. choose another artist? Whaaaa?");
            }

            switch (favouriteArtist)
            {
                case "Miley Cyrus":
                    Console.WriteLine("She came in like a wrecking ball");
                    break;
                case "Enimem":
                    Console.WriteLine("They want Shady.");
                    break;
                default:
                    Console.WriteLine("Ummm.. choose another artist? Whaaaa?");
                    break;
            }

        }
    }
}
