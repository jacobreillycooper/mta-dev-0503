using System;

namespace Arithmetic
{
    class Mathematics
    {
        public int addition(int numberOne, int numberTwo) // () means it is a method
        {
            return numberOne + numberTwo;
            // void means return no value
            // int means we HAVE to return an integer value
        }

        public int subtraction(int numberOne, int numberTwo)
        {
            return numberOne - numberTwo;
        }

        public decimal division(decimal numberOne, decimal numberTwo)
        {
            return numberOne / numberTwo;
        }
    }
}