﻿using System;
using Arithmetic;
using HumanRace;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Mathematics demo = new Mathematics();
            // HumanRace.Person reuben = new HumanRace.Person();
            Person reuben = new Person();

            reuben.firstName = "Reuben";
            reuben.eat();
            reuben.sleep();


            
            int answer = demo.addition(1, 2);
            Console.WriteLine(answer); // guess what this will write to the console?

            int differentAnswer = demo.addition(123, 123);
            Console.WriteLine(differentAnswer);

            int subtractionAnswer = demo.subtraction(10, 5);
            Console.WriteLine(subtractionAnswer);

            decimal divisionAnswer = demo.division(10m, 3m);
            Console.WriteLine(divisionAnswer); // what will the answer be here?
        }
    }
}
