using System;

namespace HumanRace
{
    class Person
    {
        public string firstName;
        public int hungerLevel;
        public int tirednessLevel;

        public void eat()
        {
            hungerLevel--;
        }

        public void sleep()
        {
            hungerLevel++;
            tirednessLevel--;
        }
    }
}