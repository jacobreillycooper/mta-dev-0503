﻿using System;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName = "Dave";
            string lastName = "Lord";
            int age = 56;

            // concatentation
            // string fullName = firstName + lastName;
            string fullName = string.Concat(firstName, lastName);
            // number 1
            Console.WriteLine("My first name is " + firstName + ", my last name is " + lastName + ". I am " + age + " years old.");

            // number 2
            Console.WriteLine($"My first name is {firstName}, my last name is {lastName}. I am {age} years old.");

            // number 3
            Console.WriteLine("My first name is {0}, my last name is {1}. I am {2} years old.", firstName, lastName, age);

            Console.WriteLine(firstName[0]);
        }
    }
}
