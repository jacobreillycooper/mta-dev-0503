using System;

namespace AnimalKingdom
{
    public class Animal
    {
        public string firstName;
        public string latinName;
        public int legs;
        public string colour;
        public void eat() 
        {
            Console.WriteLine("I am eating");
        }
        public void sleep()
        {
            Console.WriteLine("I am sleeping");
        }
    }

    public class Lion : Animal 
    {
        public void roar()
        {
            Console.WriteLine("Roar.");
        }
    } 

    public class Monkey : Animal
    {
        public void climb()
        {
            Console.WriteLine("Climbing.");
        }
    }

    public class Bird : Animal
    {
        public void chirp()
        {
            Console.WriteLine("Chirp");
        }
    }

    public class Parrot : Bird
    {
        public void sitThereQuietly()
        {
            Console.WriteLine("Quiet sounds.");
        }
    }
}