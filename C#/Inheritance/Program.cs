﻿using System;
using AnimalKingdom;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal greg = new Animal(); // we have now created a new instance of the Animal class and called it greg

            Lion simba = new Lion();

            Monkey jack = new Monkey();

            simba.firstName = "Simba";
            simba.latinName = "Panthera Leo";
            simba.roar();

            jack.firstName = "Jack";
            jack.latinName = "Cercopithecidae";
            jack.climb();

            Parrot polly = new Parrot();

            polly.latinName = "Psittaciformes";
            polly.chirp();
            polly.sitThereQuietly();

            // greg.roar(); // 'Animal' does not contain a definition for 'roar' and no accessible extension method 'roar'

            // bearded dragon
            // greg.firstName = "Greg";
            // greg.latinName = "Pogona Vitticeps";
            // greg.colour = "yellow";
            // greg.legs = 4;

            // greg.eat();
            // greg.sleep();
        }
    }
}
