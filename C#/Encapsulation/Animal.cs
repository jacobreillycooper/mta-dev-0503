using System;

namespace AnimalKingdom
{
    class Animal 
    {
        public string firstName;
        public string location = "Manchester";
        private bool manchesterBetterThanLondon = true;

        public void announceLocation()
        {
            Console.WriteLine(location);
            Console.WriteLine(manchesterBetterThanLondon);
            // manchesterBetterThanLondon is a read-only variable
        }

    }
}