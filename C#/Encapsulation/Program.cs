﻿using System;
using AnimalKingdom;

namespace Encapsulation
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal greg = new Animal();

            greg.firstName = "Greg";
            // greg.latinName = "Gregory";
            greg.location = "London";
            // when location is public, it is able to be changed by other classes
            // what about if we didn't want location to change?
            // if we don't it to be changed, then we set it to private
            greg.announceLocation();
        }
    }
}
