# C# Introduction

## What is C#?

* We use VS Code or Visual Studio for C#
* [Mac Cheatsheet for VS Code](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-macos.pdf)
* [Windows Cheatsheet for VS Code](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf)

* Programming Language that allows us to create different software application:
    * Web Applications
    * Console Applications
    * Desktop Applications
    * Mobile Applications

* C# is a great educational language... it's hard! 

* C# is a compiled language. We need to run C# each time we want to run the program
    * In [Visual Studio Code](https://code.visualstudio.com/), to create a new project:
        ```
        * Click on Terminal > New Terminal
        * Type in 'dotnet new console'
        * To check it has created correctly, we run 'dotnet run'
        ```

* Software Development -> the principles and the foundations are the same throughout each language (except a couple).. the only difference between the langauges is the syntax

```csharp
Console.WriteLine("Hello, World");
```
```js
console.log("Hello, World");
```
```java
System.out.println("Hello, World!"); // sout
```
```python
print("Hello, World")
```
```c
printf("Hello, World");
```

### What is in a C# application?

* Data Types
    * string -> handles string of text / character
    * integer -> whole number: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 
        * -2,147,483,648 to 2,147,483,647
        * What about if we want a bigger number?
            * **Long**: -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
    * Float -> A decimal number: 1.1, 1.23... 
        * A floating point number is precise up to 7 decimal places: 1.2345678
        * What about if we want to handle MORE decimal places?
            * **Double** is a decimal number, it is precise up to 14/15 decimal places
            * **Decimal** is a decimal number, it is precise up to 28 decimal places
    * Boolean -> **true or false** AKA on / off, yes / no, 1 / 0
        * True / False in other languages
    * char -> handles a single character... 'j'
        * single quotation marks ONLY

    ```csharp
    string myFirstName = "Jacob"; // always put inside of " ";
    int myIntNumber = 10; // always a whole number with a ;
    float myFloatNumber = 10.4f; // a decimal with an f;
    double myDoubleNumber = 10.4; // you don't need to put anything at the end except a ;
    bool myBoolean = true; // true or false;
    char myChar = 'J'; // single ' ';
    long myFavouriteNumber = 4; // always a whole number with a ;
    decimal myDecimalNumber = 10.4m; // you need to put m;
    // please store space for -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
    ```

* Variables
    * They are a storage place for data we want to reuse
    * Data normally comes from the user -> usernames, passwords, emails etc
    * Data can be given to the user via variables
    * Storage box so we can come back to them

    * Variables vary: the value can change at any point that you like
    ```csharp
    string myFirstName = "Jacob";
    myFirstName = "Dave";
    myFirstName = "Steve";

    int favouriteNumber = 4;
    favouriteNumber = 7;
    favouriteNumber = 33;

    int leastFavouriteNumber = 5;
    leastFavouriteNumber = 3.4f; // this will result in an error
    leastFavouriteNumber = "Steve"; // this will result in an error
    ```
    * C# a strongly-typed language. We need to specify the data type we are using for our variables

    ## C# Naming Conventions

    * Camel Casing
        * the first letter of each new word except the first word is capitalised
        * myFirstVariable; -> my is lowercased. First has a capital. Variable has a capital.
        * Creating **variables** in C#, you use camel casing

    * Pascal Casing
        * Each first letter of each word is capitalised, including the first word
        * MyFirstVariable; -> My has a capital. First has a capital. Variable has a capital.
        * File names, Classes and Namespaces in C#

    * Snake Casing
        * Primarly for Python but often ignored / forgotten
        * There should be no capitals, only underscores
        * my_first_variable

    **All Naming Conventions use cases are irrelevant if your company decides to use something else** 

    * Best Practice -> What the documentation suggests it should be used for
    * Business Practice -> What the business decides how it should be used
    * Common Practice -> bedroom coders, online solutions, someone somewhere said it could use this... **try and avoid this**

    ## Challenge / TODO

    ```csharp
    string myFirstName; // always put inside of " ";
    int myIntNumber; // always a whole number with a ;
    float myFloatNumber; // a decimal with an f;
    double myDoubleNumber;
    bool myBoolean; 
    char myChar;
    
    // Please create the above data types and let me know when you're done :-)
    // 11:07
    save the file and type: 'dotnet run' in the console
    
    ```

    ### Conditionals

    * If statements
    * If else
    * If, else if, else

    * Is this the most efficient way to check a condition?


## Operators

* Arithmetic Operators
    * +, -, /, *, %
    * Modulus %
        * 10 % 3 = 1
        * 11 % 3 = 2 
        * 12 % 3 = 0
* Assignment Operators
    * =, +=, -=, /=, *=, %=
    * int numberOne = 10;
    * numberOne += 20; // numberOne is now equal to 30
    * numberOne -= 5; // numberOne is now equal to 25
    * numberOne /= 5; // numberOne is now equal to 5
    * numberOne *= 5; // numberOne is now equal to 25

* Comparison Operators
```
    ==, <, >, <=, >=
    == Comparison check -> is the left equal to the right?
    < Less than
    > Greater than 
    <= Less than or equal to
    >= Greater than or equal to 
```

* Create either a favouriteVehicle or favouriteArtist variable with a value
* Creating an If statement that checks for a favouriteVehicle OR a favouriteArtist
* Check 1 if and 4 else ifs
* Have the else say "Just stick to Miley Cyrus"


* Logical Operators

    https://bitbucket.org/jacobreillycooper/mta-dev-0503/src/main/

* Constants
* Arrays
* Operators
* Methods
* Classes & Objects