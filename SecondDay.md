# C# Deeper dive - Day 2

# Loops

* For

    * For loop is a loop that checks against a condition, normally a length of a collection (array, list, queue, stack) or a number
    ```csharp
    int largestNumber = 10;
    for(int i = 0; i < largestNumber; i++)
    {
        Console.WriteLine(i); // 0,1,2,3,4,5,6,7,8,9
    }
    ```
    * A For loop has 3 things:
        1. **Initialiser** that cannot be accessed OUTSIDE of the For loop. Hence why we create int i = 0; 
        2. **Condition** that the for loop checks against to make sure it should still run. i < largestNumber;
        3. **Iterator** that we can increase or decrease our initialiser by. i++;
    ```csharp
    for(int i = 0; i < 10; i++)
    {
        // i = 0 is our Initialiser
        // i < 10 is our Condition
        // i++ is our Iterator
    }
    ```
    * A For loop is very structured and rigid. You can't really go wrong with it!

* For each
    * Checks the items in a collection and returns each item until there are no more items to check
    ```csharp
    foreach(string item in favouriteArtists)
    {
        Console.WriteLine(item);
    }
    ```
* While
    * The while loop checks a single condition:
        * If that condition is true, the loop will continue
    * The while loops needs two things:
        1. Condition to check against
        2. An iterator -> will cause the condition to eventually return false... if we forget the iterator, our application will crash very soon
    ```csharp
    int randomNumber = 10;
    while(randomNumber < 100) // our condition
    {
        Console.WriteLine(randomNumber);
        randomNumber++; // if we forget this line, we get an infinite loop
    }
    ```
* Do-while
    * Very similar to a while loop... with one difference!
    * The do-while is guaranteed to run at least once, no matter the condition
    ```csharp
    int randomNumber = 1000;
    do {
        Console.WriteLine("The number is less than 100");
    }
    while (randomNumber < 100)

# Arrays

* Collection of items / data that have a fixed size and cannot be **increased in C#**
* We can store multiple values inside of an array... in C#, those values have to be of the same type
* Arrays are identified by the **[]** syntax and populated in most cases with the **{}** syntax
* We start counting in arrays at 0 in C#:
    * If we have 5 items: 
        * { "Miley Cyrus", "Enimem", "50 Cent", "Hannah Montana", "Lady Gaga" }
        *       0               1       2               3               4

## What can we do to an array?

* **We can access items inside of an array:**
    * favouriteArtists[0]; // Miley Cyrus
* **We can change items inside of an array:**
    * favouriteArtists[3] = "Hugh Jackman in the Greatest Showman"; // Hannah Montana is out... long live Hugh Jackman in the Greatest Showman
* **We can see how long an array is:**
    * Console.WriteLine(favouriteArtists.Length); // 5 would be the answer... Length is length (capitalised in C#)
* **Loop through / over an array:**
    * Instead of Console.WriteLine each new item, we can write code that will do it for us
    * Loops to come!

* [Link to Arrays Project]()

# Stacks

* Very similar to Arrays in that they hold a collection of items
* Stack holds and removes elements in a LIFO (Last in, First Out) style
* To use a Stack, Queue or List, you must have the following code in your file:
    ```csharp
    using System.Collections.Generic;
    // if you don't use this code, your program will not recognise the stack, queue or list and will throw an error
    ```

## What can we do inside of a Stack?

* Creating a Stack:
    ```csharp
    Stack<string> favouriteArtists = new Stack<string>();
    ```

* Add items to a stack, whenever we want: 
    * favouriteArtists.Push("Miley Cyrus"); // will add Miley Cyrus to my stack

* Remove an item from a Stack, whenever we want:   
    * favouriteArtists.Pop(); // this will remove the last item from the Stack... you don't specify the item, it just removes the Last In

* Check the Last Item in the Stack, whenever we want:
    * Console.WriteLine(favouriteArtists.Peek()); // will show in the Console, the Last Item

* Check if an Item exists in the Stack, whenver we want:
    * Console.WriteLine(favouriteArtists.Contains("Miley Cyrus")); // true if it does, false if it doesn't

* Clear a Stack, whenever we want:
    * favouriteArtists.Clear(); // the stack is now empty

# Queues

* Hold a collection of items that work in a FIFO (First in, first out) style
* It can hold multiple values but they must be of the same type

* Create a queue:
    ```csharp
    Queue<string> favouriteArtists = new Queue<string>();
    // please use Enqueue, Dequeue, Peek, Contains and Clear
    ```

* The ability to add to our queue: **Enqueue("Miley Cyrus");**
    ```csharp
    favouriteArtists.Enqueue("Miley Cyrus"); // adds Miley Cyrus to the back of our queue
    favouriteArtists.Enqueue("Eminem"); // adds Eminem to the back of our queue
    ```

* The ability to remove from a queue: **Dequeue();**
    ```csharp
    favouriteArtists.Dequeue(); // Miley Cyrus is removed
    ```

* In the same way as Stack, we can Peek at the first item of our Queue with **Peek()**:
    ```csharp
    favouriteArtists.Enqueue("50 Cent"); // 50 Cent goes to the back of the queue
    string frontOfQueue = favouriteArtists.Peek(); // Eminem
    // Peek checks the front
    ```

* Contains is a method inside of Queue and it works the same way as Contains in Stack:
    ```csharp
    Console.WriteLine(favouriteArtists.Contains("Miley Cyrus")); // what is the answer?
    // false as Miley Cyrus was removed earlier :-)
    ```

* Clear is the same too:
    ```csharp
    favouriteArtists.Clear(); // this will empty the queue :-)
    ```

# Lists

* An ability to store a collection items, all of the same type: int, string etc
* It comes under System.Collections.Generic; which means you need that to use a list
* A list has an index [0][1] etc... and that index starts at 0! 
* Efficiency? A list is the quickest and most error-proof of the collections

## What can we do with Lists?

* Creating our list looks like this:
```csharp
List<string> favouriteArtists = new List<string>();
```

* **Add** to the end our list using: Add()
* **AddRange** to the end our list using: AddRange(); // we can add Arrays to our list

* **Insert** into a specific point of our list: Insert(); // we specify the index entry point
* **InsertRange** into a specific point of our list: InsertRange(); 

* **Remove** the first occurence of an element / item in our list with: Remove();
* **RemoveAt** an element at a specific index entry point in our list with: RemoveAt();

* **Sort** either alphabetically or numerically with: Sort();
* **ReverseSort** either alphabetically or numerically with: Reverse();

* **Clear** all items from our list with: Clear();
* **Contains** works the same way as Queues and Stacks: Contains();
* **Count** is used the same way as Stacks and Queues too

# Inheritance

* Our classes are very powerful. We can create variable templates, method templates and make full applications based off of them
* Inheritance allows us to pass traits (variables and methods) down from a parent (base )class to a child (derived) class

* [Link to the Inheritance Project]()

# Encapsulation

* We are able to hide / restrict access to variables and methods that we want to keep secure

* **public** -> allows the value / method to be accessible to everybody / everything (when accessed correctly)
* **private** -> Restricts the access to a value / method to the class that it is inside of only

* **protected** -> you can access the code in the same class, or a child class that has inherited from the parent
* **internal** -> accessible by the code's own assembly (it's own project)

* [Link to the Encapsulation Project]()

* Polymorphism