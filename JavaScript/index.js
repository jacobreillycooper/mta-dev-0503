let myFirstName = "Jacob"
let myAge = 56
let myLocation = "Wyboston"

// the old way

function birthday() {
    myAge++
    console.log(myAge)
}

function nameChange() {
    myFirstName = "Bob"
    console.log(myFirstName)
}

birthday() // running birthday / calling birthday
birthday()

nameChange() // running nameChange / calling nameChange

const ageReducer = () => {
    myAge--
    console.log(myAge)
}

const locationChange = () => {
    myLocation = "Manchester"
    console.log(myLocation)
}

ageReducer()
ageReducer()

locationChange()

let shoesOn = true // true

function shoeCheck() {

    if(shoesOn == true)
    {
        shoesOn = false
        console.log("I have taken my shoes off")
    }
    else {
        shoesOn = true
        console.log("I have put my shoes on")
    }

}

shoeCheck() // what will the output of this be?
shoeCheck() // what will the output of this be?
shoeCheck() // what will the output of this be?

let lightOn = true

function lightSwitch(lightOn) {
    if (lightOn == true) {
        console.log("Turn that light off")
        lightOn = false
    }
    else {
        console.log("Turn the light on")
        lightOn = true
    }
}

lightSwitch(lightOn)
lightSwitch(lightOn)
lightSwitch(lightOn)
lightSwitch(lightOn)

let pinNumber

function pinCheck(pinNumber) {

    if(pinNumber === 1234)
    {
        console.log("Welcome to Jacob's Bank. You have correctly entered your pin")
    }
    else if(pinNumber == 1234)
    {
        console.log("Stop entering on a keyboard. Use the numbers provided")
    }
    else {
        console.log("Go away, thief.")
    }

}

// pinCheck(2222)
// pinCheck(9876)
// pinCheck(5555)
// pinCheck(6321)
// pinCheck(9085)
// pinCheck()
pinCheck("1234") // a string
pinCheck(1234) // a number

