let lightOn = true

function lightSwitch() {
    if (lightOn == true) {
        console.log("Turn that light off")
        lightOn = false
    }
    else {
        console.log("Turn the light on")
        lightOn = true
    }
}

lightSwitch(lightOn)
lightSwitch(lightOn)
lightSwitch(lightOn)
lightSwitch(lightOn)